<?php 
class Desconto5Itens implements Desconto {
    private $proximoDesconto;

    public function desconto(Orcamento $orcamento) {
        if (count($orcamento->getItens()) >= 5 ){
            return $orcamento->getValor() * 0.1;
        }
        
        return $this->proximoDesconto->desconto($orcamento);
    }

    /**
     * Get the value of proximoDesconto
     */ 
    public function getProximoDesconto()
    {
        return $this->proximoDesconto;
    }

    /**
     * Set the value of proximoDesconto
     *
     * @return  self
     */ 
    public function setProximoDesconto($proximoDesconto)
    {
        $this->proximoDesconto = $proximoDesconto;

        return $this;
    }
}