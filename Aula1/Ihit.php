<?php
class Ihit extends TemplateDeImpostoCondicional {

  public function deveUsarOMaximo(Orcamento $orcamento) {
    $noOrcamento = Array();

    foreach($orcamento->getItens() as $item) {
      if(in_array($item->getNome(),$noOrcamento)) return true;
      else $noOrcamento[] = $item->getNome();
    }

    return false;
  }
 
  public function taxacaoMaxima(Orcamento $orcamento) { 
    return $orcamento->getValor() * 0.13 + 100;
  }
  public function taxacaoMinima(Orcamento $orcamento) {
    return $orcamento->getValor() * (0.01 * count($orcamento->getItens()));
  }
}