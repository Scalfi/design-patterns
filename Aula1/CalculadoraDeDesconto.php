<?php
    class CalculadoraDeDesconto {
        public function desconto(Orcamento $orcamento) {
            $desconto5Itens = new Desconto5Itens();
            $desconto500Reais = new Desconto500Reais();
            $desconto300Reais = new Desconto300Reais();
            $semDesconto = new SemDesconto();

            $desconto5Itens->setProximoDesconto($desconto500Reais);
            $desconto500Reais->setProximoDesconto($desconto300Reais);
            $desconto300Reais->setProximoDesconto($semDesconto);

            $valor = $desconto5Itens->desconto($orcamento);

            return $valor;


        }
    }