<?php 

    class Orcamento {
      
        private $valor;
        private $itens;

        function __construct($novoValor)
        {  
            $this->valor = $novoValor;
            $this->itens =  [];
        }

        public function addItens(Item $novoItem) {
            $this->itens[] = $novoItem;
        }

        /**
         * Get the value of valor
         */ 
        public function getValor()
        {
                return $this->valor;
        }

        /**
         * Set the value of valor
         *
         * @return  self
         */ 
        public function setValor($valor)
        {
                $this->valor = $valor;

                return $this;
        }

        /**
         * Get the value of itens
         */ 
        public function getItens()
        {
                return $this->itens;
        }

        /**
         * Set the value of itens
         *
         * @return  self
         */ 
        public function setItens($itens)
        {
                $this->itens = $itens;

                return $this;
        }
    }