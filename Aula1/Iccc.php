<?php 
    class Iccc implements Imposto {
        public function calcula(Orcamento $orcamento)
        {
            $orcamento = $orcamento->getValor();

            if ($orcamento < 1000) {
                $calculo = $orcamento * 0.05;
            } else if ($orcamento >= 1000 && $orcamento <= 3000) {
                $calculo = $orcamento * 0.07;
            } else if ( $orcamento > 3000) {
                $calculo = ($orcamento * 0.08) + 30;
            }

            return $calculo;
        }
    }