<?php
    require_once("Orcamento.php");
    require_once("CalculadoraDeImpostos.php");
    require_once("InterfaceImposto.php");
    require_once("TemplateDeImpostoCondicional.php");
    require_once("Icms.php");
    require_once("Iss.php");
    require_once("Kcv.php");
    require_once("Iccc.php");
    require_once("Icpp.php");
    require_once("Ihit.php");

    require_once("CalculadoraDeDesconto.php");
    require_once("InterfaceDesconto.php");
    require_once("Desconto5Itens.php");
    require_once("Desconto500Reais.php");
    require_once("Desconto300Reais.php");
    require_once("SemDesconto.php");

    require_once("Item.php");



    $reforma = new Orcamento(301);

    $calculadoDeImpostos = new CalculadoDeImpostos();

    $calculadoDeDesconto = new CalculadoraDeDesconto();

    $itens =  new Item();


    echo $calculadoDeImpostos->calcula($reforma, new Icms());
    
    echo "<br />";

    echo $calculadoDeImpostos->Calcula($reforma, new Iss());

    echo "<br />";

    echo $calculadoDeImpostos->Calcula($reforma, new Kcv());
    
    echo "<br />";

    echo $calculadoDeImpostos->Calcula($reforma, new Iccc());

    echo "<br />";


    echo $calculadoDeImpostos->Calcula($reforma, new Icpp());

    echo "<br />";

    echo $calculadoDeImpostos->Calcula($reforma, new Ihit());

    echo "<br />";

    echo "-----------------------------";
    echo "<br />";

    echo "Teste Descontos";
    echo "<br />";

  
    
    $reforma->addItens($itens->setNome("Tijolo"),$itens->setValor(250));
    $reforma->addItens($itens->setNome("Telha"),$itens->setValor(250));
    $reforma->addItens($itens->setNome("Telha"),$itens->setValor(250));
    $reforma->addItens($itens->setNome("Telha"),$itens->setValor(250));
   
    echo $calculadoDeDesconto->desconto($reforma);

