<?php
    Class Arrojado implements Investimento {
        public function investimento(ContaBancaria $conta)
        {
            $chance = mt_rand(1,100);
            $saldo = $conta->getSaldo();

            if ($chance <= 20) {
                return $saldo * 0.005;
            } else if ($chance > 20 && $chance <= 50) {
                return $saldo * 0.003;
            } else {
                return $saldo * 0.006;
            } 
        }
    }