<?php
    Class Conservador implements Investimento {
        public function investimento(ContaBancaria $conta)
        {
            return $conta->getSaldo() * 0.08;
        }
    }