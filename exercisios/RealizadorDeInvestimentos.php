<?php
    class RealizadorDeInvestimentos
    {
        public function investimento(ContaBancaria $conta, Investimento $investimento ) {
            $response = $investimento->investimento($conta);

            return $response * 0.75;
        }
    }