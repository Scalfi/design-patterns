<?php 

require_once("ContaBancaria.php");
require_once("InterfaceInvestimento.php");
require_once("RealizadorDeInvestimentos.php");
require_once("Moderado.php");
require_once("Conservador.php");
require_once("Arrojado.php");

$realizadorDeInvestimento = new RealizadorDeInvestimentos();
$conta = new ContaBancaria();
$conservador = new Conservador();
$moderado = new Moderado();
$arrojado = new Arrojado();


$conta->setSaldo(200);

echo $realizadorDeInvestimento->investimento($conta, $conservador);

echo "<br>";

echo $realizadorDeInvestimento->investimento($conta, $moderado);

echo "<br>";

echo $realizadorDeInvestimento->investimento($conta, $arrojado);

echo "<br>";

