<?php
    Class Moderado implements Investimento {
        public function investimento(ContaBancaria $conta)
        {
            $chance = mt_rand(1,2);
            $saldo = $conta->getSaldo();

            if ($chance == 1) {
                return $saldo * 0.025;
            } else {
                return $saldo * 0.007;
            }
        }
    }